<h1>Tentang Aplikasi</h1><br>
<b>NGUMBARA - Ngumbara di Bandung Juara</b> merupakan aplikasi mobile untuk perangkat android. Aplikasi ini berfungsi untuk memberikan informasi mengenai tempat wisata yang ada di wilayah Kota Bandung. Pada aplikasi ini memiliki fitur antara lain : <br>
<ol>
<li>Pencarian tempat wisata</li>
<li>Daftar tempat wisata rekomendasi</li>
<li>Informasi detail tempat wisata seperti : harga tiket, jenis tempat wisata, fasilitas, dll.</li>
<li>Petunjuk arah menuju tempat wisata.</li>
<li>Informasi jarak dan waktu tempuh menuju tempat wisata</li>
</ol>
<br>
Pada gambar di bawah ini merupakan snapshot dari aplikasi <b>NGUMBARA - Ngumbara di Bandung Juara</b> : <br>
<table>
<tr>
<td><img src="https://nextwarestudio.com/1.png"></td>
<td><img src="https://nextwarestudio.com/2.png"></td>
<td><img src="https://nextwarestudio.com/3.png"></td>

</tr>
<tr>
<td><img src="https://nextwarestudio.com/4.png"></td>
<td><img src="https://nextwarestudio.com/5.png"></td>
<td><img src="https://nextwarestudio.com/6.png"></td>
</tr>
</table>