package com.uasmobprog.rahmanabdulrazak.ngumbara.rest;

import com.uasmobprog.rahmanabdulrazak.ngumbara.models.ResultResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("/maps/api/place/nearbysearch/json")
    Call<ResultResponse> getNearbyResults(@Query("types") String types, @Query("location") String location, @Query("radius") Integer radius, @Query("key") String key);
}
