package com.uasmobprog.rahmanabdulrazak.ngumbara.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResultResponse {

    @SerializedName("results")
    private List<Results> resultsList;

    public List<Results> getResultsList() {
        return resultsList;
    }

    public void setResultsList(List<Results> resultsList) {
        this.resultsList = resultsList;
    }
}
