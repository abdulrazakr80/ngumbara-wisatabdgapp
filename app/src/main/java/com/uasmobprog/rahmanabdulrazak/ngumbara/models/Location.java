package com.uasmobprog.rahmanabdulrazak.ngumbara.models;


public class Location {

    private String lat, lng;

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
