package com.uasmobprog.rahmanabdulrazak.ngumbara.models;


public class Results {

    private Geometry geometry;
    private String icon, name, vicinity;

    public Geometry getGeometry() {
        return geometry;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getVicinity() {
        return vicinity;
    }
}
