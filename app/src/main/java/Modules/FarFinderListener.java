package Modules;

import java.util.List;


public interface FarFinderListener {
    void onFarFinderStart();
    void onFarFinderSuccess(String distance);
}
